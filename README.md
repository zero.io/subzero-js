# [ZERO.IO](https://zero.io) Web3 Utilities

ZERO is a videogames metachain based on substrate. Game creators build next generation games and networks utilizing subzero as a relaychain to build interoperable games with economics breaking free from silos, fostering sustainable economics for creators and gamers.

## Build + Publish

`yarn`
`yarn build`

## License

subzero-js is [GPL 3.0](./LICENSE) licensed.
